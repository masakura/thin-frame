# フレーム落ちを再現する方法
1. CPU クロックを下げた仮想環境内で動作させる
2. 重たい処理を並行して走らせる
3. 一フレームあたりの描画回数を増やす
4. フレームを意図的にスキップする
5. Chrome DevTools Remote Debug を使う


## 1. CPU クロックを下げた仮想環境内で動作させる
VirtualBox などの仮想環境は CPU のコア数を減らしたり、クロックを下げたりする機能がある。これを使うと、意図的に遅いマシンを再現することができる。


## 2. 重たい処理を並行して走らせる
無限ループをするプログラムを書いて、それを何本も走らせて CPU に負荷をかける。その分、ブラウザーが遅くなる。

[Node.js LTS](https://nodejs.org/) をインストールして、コマンドプロンプトから以下を実行するだけで、ひとつの CPU コアに負荷を 100% かけられる。 

```
> node -e 'while (true) {}'
```


## 3. 一フレームあたりの描画回数を増やす
以下のように、一フレームあたり一回描画しているとする。

```javascript
const tick = () => {
  game.update();
  
  requestAnimationFrame(() => tick());
};
tick();
```

描画回数を増やすことで、一フレーム内で描画が完了しないようにして、意図的にフレーム落ちを起こさせる。

```javascript
const tick = () => {
  game.update();
  game.update();
  game.update();
  game.update();
  // ... コマ落ちするまで増やす
  
  requestAnimationFrame(() => tick());
};

tick();
```


## 4. フレームを意図的にスキップする
3 とは逆に、フレームを間引いてスキップさせる。描画回数が減るので、見た目上フレーム落ちになる。

```javascript
let count = 0;

const tick = () => {
  count = (count + 1) % 2;
  // 二回に一回しか描画メソッドを呼ばない
  if (count === 0) game.update();
  
  game.update()
  
  requestAnimationFrame(() => tick());
};

tick();
```


## 5. Chrome DevTools Remote Debug を使う
Android 限定の方法になるが、Chrome DevTools Remote Debug で Android Chrome にアタッチする方法がある。パソコンに画面転送される関係か、その分フレームレートが 10fos くらいまで落ち込む。

1. Android をパソコンに USB デバッグモードでつなぐ
2. Android Chrome でウェブアプリを開く
3. パソコンの Chrome DevTools から、Remote Devices で Android の Chrome を選択
4. パソコンに Android の画面が表示されたら OK! (ダメな場合はスクリーンキャストを有効に)

参考: [Android 端末のリモート デバッグを行う](https://developers.google.com/web/tools/chrome-devtools/remote-debugging/?hl=ja9)

> スクリーンキャストはフレームレートに悪影響を及ぼします。スクロールやアニメーションを調整するときには、スクリーンキャストを無効にすると、ページのパフォーマンスをより正確に把握できます。


## ES6
新しい JavaScript の規格 ECMAScript 6 では、関数の書き方が追加された。(古い書き方が動かなくなるわけではないが、トラブルが起きやすいので新しい書き方が生まれた)

```javascript
// ECMAScript 5 (古い規格でも動作する)
function tick() {
  game.update();
  
  requestAnimationFrame(function () {
    tick();
  });
}

tick();
```

```javascript
// ECMAScript 6 (新しい書き方)
const tick = () => {
  game.update();
  
  requestAnimationFrame(() => tick());
};

tick();
```

このケースでは問題は出ないが、上と下で完全に同じ挙動というわけではないことは覚えておく。


## ストラテジーパターン
3 と 4 の方法はソースコードを書き換える必要がある。テストをするときにコードを書き換えて、終わったら戻す方法はできれば避ける。なぜならば、よく忘れるから! 戻し方を間違うから!

```javascript
// 通常通り
const normal = action => action();

// 五回連続で呼び出す
const multiple = action => {
  action();
  action();
  action();
  action();
};

const thin = normal; // いつもはこの行を有効に
// const thin = multiple; // テストの時だけこちら側を有効に

const tick = () => {
  thin(() => game.update());
  
  requetAnimationFrame(() => tick());
};

tick();
```

## サンプルコード
[サンプルを実行する。](https://masakura.gitlab.io/thin-frame/index.html)

サンプルコードは 4 の方法をストラテジーパターンを使って、動作中に切り替えられるようにしたもの。

* `通常` - フレームをスキップしません。通常通り 60fps で動作します。
* `二回に一回フレームをスキップする` - ちょうど 30fps になります。
* `ランダムでフレームをスキップする` - スキップするかどうかをランダムで決定します。カクカクします。

どれを選んでも、経過時間 (画面左から右に到達するまでの時間) はあまり変わらないことが確認できます。