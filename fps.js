export default class Fps {
    constructor() {
        this.frames = [];
    }

    add() {
        this.frames.push(performance.now());
    }

    get rate() {
        if (this.frames.length <= 0) return 0;

        const threshold = performance.now() - 1000;

        let first = this.frames[0];
        while (first < threshold) {
            this.frames.shift();
            first = this.frames[0];
        }

        return this.frames.length;
    }
}