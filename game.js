import {
    OrthographicCamera,
    PerspectiveCamera,
    Scene,
    WebGLRenderer,
    Sprite,
    SpriteMaterial,
    BoxBufferGeometry,
    TextGeometry,
    MeshBasicMaterial,
    Mesh
} from 'https://cdnjs.cloudflare.com/ajax/libs/three.js/92/three.module.js';
import Fps from './fps.js';
import StopWatch from './stopwatch.js';

const halfSize = (size) => {
    return {
        width: size.width / 2,
        height: size.height / 2,
    };
};

const factory = {
    camera: {
        perspective(size) {
            const camera = new PerspectiveCamera(45, size.width / size.height);
            camera.position.set(0, 0, 1000);
            return camera;
        },

        orthographic(size) {
            const half = halfSize(size);
            const camera = new OrthographicCamera(-half.width, half.width,half.height, -half.height, 1, 1000);
            camera.position.set(0, 0, 1000);
            return camera;
        },
    },
    mesh: {
        mesh() {
            const geometry = new BoxBufferGeometry(128, 128, 128);
            const material = new MeshBasicMaterial({color: 0xffff00});
            return new Mesh(geometry, material);
        },
        sprite() {
            const material = new SpriteMaterial({color: 0xaa00ee});
            const sprite = new Sprite(material);
            sprite.scale.set(128, 128, 1);
            return sprite;
        },
    },
};

export default class Game {
    constructor($canvas) {
        const size = {
            width: $canvas.width,
            height: $canvas.height,
        };

        const renderer = new WebGLRenderer({canvas: $canvas});
        renderer.setPixelRatio(window.devicePixelRatio);
        renderer.setSize(size.width, size.height);

        const scene = new Scene();

        const camera = factory.camera.orthographic(size);

        const mesh = factory.mesh.sprite();
        scene.add(mesh);

        this._renderer = renderer;
        this._scene = scene;
        this._camera = camera;
        this._mesh = mesh;
        this._size = size;

        this._fps = new Fps();
        this._stopwatch = new StopWatch();
    }

    get fps() {
        return this._fps.rate;
    }

    get stopwach() {
        return this._stopwatch.current;
    }

    update(step) {
        this._fps.add();

        this.moveMesh(step);

        this._renderer.render(this._scene, this._camera);
    }

    moveMesh(step) {
        let x = this._mesh.position.x;
        x += this._size.width / 2;
        x = x + step / 4;
        if (x > this._size.width) {
            this._stopwatch.restart();
            x = x % this._size.width;
        }
        x -= this._size.width / 2;

        this._mesh.position.x = x;
    }
}