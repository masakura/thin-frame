import Game from './game.js';
import thin from './thin.js';

const game = new Game(document.querySelector('#canvas'));

const thinStrategy = {
    $el: document.querySelector('#thinStrategy'),
    current: thin.normal,
    /**
     * 選択されたコマ落ち用の処理名を返します。
     * @returns {string}
     */
    get strategyName() {
        return this.$el.value;
    },
    /**
     * ドロップボックスの選択状態が切り替わったらコマ落ち処理を切り替える
     */
    initialize() {
        this.$el.addEventListener('click', () => this.select());
    },
    /**
     * ドロップボックスの選択状態に合わせてコマ落ち処理を切り替える
     */
    select() {
        this.current = thin.find(this.strategyName);
    },
    /**
     * 処理を呼び出す
     * @param action
     */
    update(action) {
        this.current(action);
    }
};
thinStrategy.select();
thinStrategy.initialize();

const fpsView = {
    $el: document.querySelector('#fps'),
    update(fps) {
        this.$el.innerHTML = fps;
    }
};

const stopwatchView = {
    $el: document.querySelector('#stopwatch'),
    update(stopwatch) {
        if (stopwatch < 1500) return;
        this.$el.innerHTML = stopwatch;
    }
};

const tick = () => {
    thinStrategy.update(step => game.update(step));

    // FPS と経過時間 (左から右に到達するまでの時間) を更新
    fpsView.update(game.fps);
    stopwatchView.update(game.stopwach);

    requestAnimationFrame(() => tick());
};

tick();
