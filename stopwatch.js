export default class StopWatch {
    constructor() {
        this.start = 0;
    }

    restart() {
        this.start = performance.now();
    }

    get current() {
        return performance.now() - this.start;
    }
};