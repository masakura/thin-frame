let timesOfHalf = 0;
const counter = {
    prev: 0,
    /**
     * 前回のフレームからの経過時間を返します。
     * @returns {number}
     */
    step() {
        const current = performance.now();
        const diff = this.prev > 0 ? (current - this.prev) : 0;
        this.prev = current;
        return diff;
    }
};

/**
 * フレームを間引くストラテジー。
 */
export default {
    /**
     * フレームを間引きません。通常の動作です。
     * @param action
     */
    normal(action) {
        action(counter.step());
    },

    /**
     * 二回に一回フレーム処理をスキップします。半分にコマ落ちした状態になります。
     * @param action
     */
    half(action) {
        timesOfHalf = (timesOfHalf + 1) % 2;

        if (timesOfHalf === 0) return;

        action(counter.step());
    },

    /**
     * ランダムでフレーム処理をスキップします。平均すると 20 fps くらいになります。
     * @param action
     */
    random(action) {
        const random = Math.floor(Math.random() * 3);

        if (random > 0) return;

        action(counter.step());
    },

    find(name) {
        switch (name) {
            case 'normal': return this.normal;
            case 'half': return this.half;
            case 'random': return this.random;
            default: throw new Error(`Unknown ${name}`);
        }
    }
};